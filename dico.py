dico_de_correspondance = {}

dico_de_correspondance['addresse_titulaire'] = "address"
dico_de_correspondance['carrosserie'] = "carrosserie"
dico_de_correspondance['categorie'] = "categorie"
dico_de_correspondance['couleur'] = "couleur"
dico_de_correspondance['cylindree'] = "cylindree"
dico_de_correspondance['date_immatriculation'] = "date_immat"
dico_de_correspondance['denomiation_commerciale'] = "denomination"
dico_de_correspondance['energie'] = "energy"
dico_de_correspondance['prenom'] = "firstname"
dico_de_correspondance['immatriculation '] = "immat"
dico_de_correspondance['marque'] = "marque"
dico_de_correspondance['nom'] = "name"
dico_de_correspondance['places'] = "places"
dico_de_correspondance['poids'] = "poids"
dico_de_correspondance['puissance'] = "puissance"

#Traitement spécial pour ces cases
dico_de_correspondance['type'] = "type_variante_version"
dico_de_correspondance['variante'] = "type_variante_version"
dico_de_correspondance['version'] = "type_variante_version"

dico_de_correspondance['vin'] = "vin"







