import csv
from dico import *
import os

def decoupage(chaine_cara):
    return chaine_cara.split(',')


listing_dico = []
listing_value=[]

def writeInCsv(row,dictionnaire): 
        new_row = {}
        for key, old_key in dictionnaire.items(): #key = nouvelle clé, old_key = valeur , .items() => parmi les clé/valeurs
            new_row[key] = row[old_key]
            if key in special_key:
                new_row[key] = decoupage(row['type_variante_version'])[special_key.index(key)]
                #ici, si la clé du dico fait partie de 'type_variante_version alors on traite uniquement
                #le cas que l'on désire
        return new_row

if os.path.exists('new_auto'):
    os.remove('new_auto.csv')

with open('auto.csv',newline='') as csvfile:
    reader = csv.DictReader(csvfile,delimiter='|')
    for ligne in reader:
        listing_value.append(ligne)


with open('new_auto.csv','w',newline='')as csvfile:
    fieldnames = []
    fieldnames = dico_de_correspondance.keys()
    writer = csv.DictWriter(csvfile,fieldnames=fieldnames,delimiter=';')

    
    special_key = ['type','variante','version']
    writer.writeheader()
    
    for row in listing_value:
        writer.writerow(writeInCsv(row,dico_de_correspondance))
    
