import unittest
from change_name import writeInCsv
from change_name import decoupage
from dico import *
from dico_test import *


class TestChangeNameFunctions(unittest.TestCase):
   
   def testsWriteCsv(self):   
        self.assertEqual(writeInCsv({'col1':'Solo','col2':'Han'},dico_test),{'nom':'Solo','prenom':'Han'})
        self.assertEqual(writeInCsv({'col1':'Bain','col2':'Quenaubie'},dico_test),{'nom':'Bain','prenom':'Quenaubie'})
        self.assertEqual(writeInCsv({'col1':'Delahaye','col2':'Samy'},dico_test),{'nom':'Delahaye','prenom':'Samy'})

   def testDecoupage(self):
      self.assertEqual(decoupage('azerty,le,petit'),( ['azerty', 'le', 'petit'] ))
      self.assertEqual(decoupage('n,i,c,o,l,a,s'),( ['n', 'i', 'c', 'o', 'l', 'a', 's'] ))
      self.assertEqual(decoupage('az;az'),( ['az;az'] ))